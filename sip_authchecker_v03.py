#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  sip_authchecker_v02.py
#
#  Copyright 2016 Marek Papco
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Program takes output of tcpdump in txt and checks provided responses
# in INVITE and REGISTER methods against provided password according
# to RFC7616.
# Supported digest algorithms: MD5
# Unsupported: 
#   <username*> - non ABNF characters used in username presented in 
#               extended notation
#   <qop=auth-int> - message intergrity protection
#   <userhash=True> - hashed usernames
#   <nc> - nonce count (might be used in response)
#   <qop> - quality of protection (might be used in response)
#
#
# Changelog:
# + added algorithms SHA256, SHA512
#

import sys
import hashlib
import argparse
import re

parser = argparse.ArgumentParser(description='Check hash responses in SIP messages against provided password')
parser.add_argument('-v', '--verbose', help='increase output verbosity', action="store_true", dest='v')
parser.add_argument('-i', metavar='input_file',nargs='?',type=argparse.FileType('rt'), required=True, help='input SIP trace (txt file)')

args = parser.parse_args()
infile = args.i
verbose = args.v
m_list = []
password = None

class SIPMessage():
    def __init__(self, mname, callid, cseq, uname, realm, nonce, uri, response, alg):
        if verbose : print ("[ClassManager] Calling " + mname + " constructor")
        self.mname = mname
        self.callid = callid
        self.cseq = cseq
        self.uname = uname
        self.realm = realm
        self.nonce = nonce
        self.cnonce = None
        self.uri = uri
        self.response = response
        self.alg = alg
        self.cresponse = None
        
    def setcresponse(self, cresponse):
        SIPMessage.cresponse = cresponse
    
    def setcnonce(self, cnonce):
        SIPMessage.cnonce = cnonce
    
    def __del__(self):
        class_name = self.__class__.__name__
        if verbose : print ("[ClassManager] " + class_name + " " + self.mname + " destroyed")
    

def main(args):
    
    password = ask_password()
    parse_infile(infile, verbose)
    compute_response(password, m_list)
  
    return 0
    
def parse_infile(infile,verbose):
    in_f = infile.readlines()
    infile.close()
    
    i = 0
      
    if (verbose): print ("[PARSE_INFILE] Input file loaded in memory")
  
    for line in in_f:
        line=line.strip() # removes white characters from line
        #print (line)
        i += 1 # remember line
        match = re.match(r'REGISTER|INVITE',line)
        if (match):
            method = match.group(0)
            if verbose: print ("[PARSE_INFILE] Method -> " + match.group(0))
            parse_method(in_f,i,verbose)
          
  
def parse_method(in_f,line_i,verbose):
 
    callid = None
    cseq = None
    method = None
    uname = None
    realm = None
    nonce = None
    cnonce = None
    uri = None
    response = None
    auth_flag = False
    algorithm = None
   
    if (verbose): print ("[PARSE_METHOD] Parsing method")
 
    while (in_f[line_i].strip() != ""):
        #print (in_f[line_i].rstrip())
        in_f[line_i] = in_f[line_i].lstrip()
        # parse Call-ID header
        if ('Call-ID:' in in_f[line_i]):
            match = re.match(r'Call-ID:\s+(\S+)\s+|i:\s+(\S+)\s+',in_f[line_i])
            if verbose : print ("[PARSE_METHOD]    Call ID -> " + match.group(1))
            callid = match.group(1)
        # parse CSeq header
        elif ('CSeq:' in in_f[line_i]):
            match = re.match(r'CSeq:\s+(\S+)\s+(\S+)',in_f[line_i])
            if verbose : print ("[PARSE_METHOD]    CSeq -> " + match.group(1))
            #if verbose : print ("[PARSE_METHOD] Method -> " + match.group(2))
            cseq = match.group(1)
            mname = match.group(2)
        # parse Authorization header
        elif ('Authorization:' in in_f[line_i]):
            match = re.match(r'(Authorization:|Proxy-Authorization:)\sDigest\s(.+)', in_f[line_i])
            if verbose : print ("[PARSE_METHOD]    Auth header -> " + match.group(0))
            auth_flag = True
            credentials = []
            credentials = re.split('\s',match.group(2))
            if (len(credentials) > 2):
                for item in credentials:
                    if ('username' in item):
                        match = re.search('username=\"(\S+)\"', item)
                        uname = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     username: " + uname)
                    elif ('realm' in item):
                        match = re.search('realm=\"(\S+)\"', item)
                        realm = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     realm: " + realm)
                    elif ('nonce' in item):
                        match = re.search('nonce=\"(\S+)\"', item)
                        nonce = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     nonce: " + nonce)
                    elif ('cnonce' in item):
                        match = re.search('cnonce=\"(\S+)\"', item)
                        cnonce = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     nonce: " + cnonce)
                    elif ('uri' in item):
                        match = re.search('uri=\"(\S+)\"', item)
                        uri = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     uri: " + uri)
                    elif ('response' in item):
                        match = re.search('response=\"(\S+)\"', item)
                        response = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     response: " + response)
                    elif ('algorithm' in item):
                        match = re.search('algorithm=(\S+)', item)
                        algorithm = match.group(1)
                        if verbose : print ("[PARSE_METHOD]     algorithm: " + algorithm)
                    else:
                        print ("[PARSE_METHOD]  Parameter [" + item + "] unknown!" )
        
        else:
            if verbose : print ("[PARSE_METHOD] NA -> " + in_f[line_i].rstrip())
    
        line_i += 1 # increase line index
    
    if verbose : print ("[PARSE_METHOD] End of method!\n")
    
    if (auth_flag):
        # if 'algorithm' parameter not present, set to MD5
        if (algorithm == None) : 
            algorithm='MD5'
            if verbose : print ("[PARSE_METHOD] No algorithm parameter found, default to MD5 ")
        if verbose : print ("[PARSE_METHOD] Calling [CREATE_METHOD]\n")
        method_obj = create_method(mname, callid, cseq, uname, realm, nonce, uri, response, algorithm)
        if (cnonce) : method_obj.set_cnonce(cnonce)
    else:
        if verbose : print ("[PARSE_METHOD] No Authorization header found, skipping method\n")
    #if verbose : print ("[PARSE_METHOD] End of method!\n")
    
    return 0

def create_method(mname, callid, cseq, uname, realm, nonce, uri, response, algorithm):
    if verbose : print ("[CREATE_METHOD] Checking if mandatory parameters are initialized.")
    if (callid == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"Call-ID\" not found!")
        return -1
    elif(cseq == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"Cseq\" not found!")
        return -1
    elif(mname == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"Method name\" not found!")
        return -1
    elif(uname == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"uname\" not found!")
        return -1
    elif(realm == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"realm\" not found!")
        return -1
    elif(nonce == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"nonce\" not found!")
        return -1
    elif(uri == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"uri\" not found!")
        return -1
    elif(realm == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"Cseq\" not found!")
        return -1
    elif(algorithm == None) : 
        print ("[CREATE_METHOD] Mandatory parameter \"algorithm\" not found!")
        return -1
    else:
        # initialize objects
        if verbose : print ("[CREATE_METHOD] Mandatory parameters test successful, initializing method object.")
        m_list.append(SIPMessage(mname, callid, cseq, uname, realm, nonce, uri, response, algorithm))
    
    return 0
 
def ask_password():
    print ("Write password then press enter")
    secret = input("secret:")
    if verbose : print ("[ASK_PASSWORD] Password: \"" + secret + "\"\n")
    
    return secret

def compute_response(secret, m_list):
    for method in m_list:
        print ("\n[COMPUTE RESPONSE] Computing response for " + 
        getattr(method, 'mname') + ' CSeq: ' +  getattr(method, 'cseq') + 
        " Call-ID: " + getattr(method, 'callid'))
        
        # Check for session/non-session algorithm
        if('-sess' in getattr(method, 'alg')):
            if verbose : print ("[COMPUTE RESPONSE]     <algorithm>-sess defined") 
            # check if cnonce defined
            if (getattr(method, 'cnonce') == None):
                print ("[COMPUTE RESPONSE] Error: Parameter \"cnonce\" not found  \
                while parameter \"<algorithm>-sess\" is!")
                exit (1)
            else:
                # <ALG>-sess -> A1 = H (uname:realm:pass:nonce:cnonce)
                A1 = getattr(method, 'uname') + ':' + getattr(method, 'realm') + \
                ':' + secret + ':' + getattr(method, 'nonce') + ':' + \
                getattr(method, 'cnonce')
        else:
            # A1 = uname + ':' + realm + ':' + upass
            A1 = getattr(method, 'uname') + ':' + getattr(method, 'realm') + \
            ':' + secret 
        if verbose : print ("[COMPUTE RESPONSE]    A1: ", A1) 
        
        if ('MD5' in getattr(method, 'alg')):
            # HA1 
            HA1 = hashlib.md5(A1.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA1: ", HA1.hexdigest()) 
            #A2 = method + ":" + ruri
            A2 = getattr(method, 'mname') + ':' + getattr(method, 'uri')
            if verbose : print ("[COMPUTE RESPONSE]    A2: ", A2)
            # HA2
            HA2 = hashlib.md5(A2.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA2: ", HA2.hexdigest())
        elif ('SHA-256' in getattr(method, 'alg')):
            # HA1 
            HA1 = hashlib.sha256(A1.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA1: ", HA1.hexdigest()) 
            #A2 = method + ":" + ruri
            A2 = getattr(method, 'mname') + ':' + getattr(method, 'uri')
            if verbose : print ("[COMPUTE RESPONSE]    A2: ", A2)
            # HA2
            HA2 = hashlib.sha256(A2.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA2: ", HA2.hexdigest())
        elif ('SHA-512-256' in getattr(method, 'alg')):
            # not sure about this
            # HA1 
            HA1 = hashlib.sha512(A1.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA1: ", HA1.hexdigest()) 
            #A2 = method + ":" + ruri
            A2 = getattr(method, 'mname') + ':' + getattr(method, 'uri')
            if verbose : print ("[COMPUTE RESPONSE]    A2: ", A2)
            # HA2
            HA2 = hashlib.sha512(A2.encode())
            if verbose : print ("[COMPUTE RESPONSE]    HA2: ", HA2.hexdigest())
        else:
            print ("[COMPUTE RESPONSE] Error: Algorithm  not defined!")
            exit (1)
        
        # response = ha1.hexdigest() + ':' + nonce + ':' + ':' + nc + 
        #            ':' + cnonce + ':' + ':' + qop + ':' + ':'+ ha2.hexdigest()
        # response = ha1.hexdigest() + ':' + nonce + ':' + ha2.hexdigest()
        if (getattr(method, 'cnonce')):
            cresponse = HA1.hexdigest() + ':' + getattr(method, 'nonce') + ':' + ':' + getattr(method, 'cnonce') + ':' + HA2.hexdigest()
        else:
            cresponse = HA1.hexdigest() + ':' + getattr(method, 'nonce') + ':' + HA2.hexdigest()
        if verbose : print ("[COMPUTE RESPONSE]    Composed response: ", cresponse) 
        
        # hresponse = hashlib.H(response.encode())
        if ('MD5' in getattr(method, 'alg')):
            hresponse =  (hashlib.md5(cresponse.encode())).hexdigest()
        elif ('SHA-256' in getattr(method, 'alg')):
            hresponse =  (hashlib.sha256(cresponse.encode())).hexdigest()
        elif ('SHA-512' in getattr(method, 'alg')):
            hresponse =  (hashlib.sha512(cresponse.encode())).hexdigest()
        else:
            print ("[COMPUTE RESPONSE] Error: Algorithm  not recognized!")
            exit (1)
            
        method.setcresponse(hresponse)
        print ("[COMPUTE RESPONSE]    Username:                 ", getattr(method, 'uname'))
        print ("[COMPUTE RESPONSE]    Computed hashed response: ", hresponse)
        print ("[COMPUTE RESPONSE]    Sent response:            ", getattr(method, 'response'))
        
    return 0
        
        

if __name__ == '__main__':
    sys.exit(main(sys.argv))
