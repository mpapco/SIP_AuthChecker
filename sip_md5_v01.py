#! /usr/bin/python

#
# Author: Marek Papco
#

# Program counts MD5 repsonse used in SIP authentication


import hashlib,sys

def main ():
    uname = "102"
    upass = "102"
    realm = "10.1.1.111"
    method = "INVITE"
    ruri = "sip:101@10.1.1.111"
    nonce = "V0Cy8VdAscWMUTVxLY425HK8tCPnGT58"

    a1 = uname + ":" + realm + ":" + upass
    print ("a1:" ,a1)
    ha1 = hashlib.md5(a1.encode())
    print ("ha1:" ,ha1.hexdigest())

    a2 = method + ":" + ruri
    print ("a2:" , a2)
    ha2 = hashlib.md5(a2.encode())
    print ("ha2:" , ha2.hexdigest())

    response = ha1.hexdigest() + ":" + nonce + ":" + ha2.hexdigest()
    hresponse = hashlib.md5(response.encode())
    print ("response:" ,hresponse.hexdigest())

main ()
