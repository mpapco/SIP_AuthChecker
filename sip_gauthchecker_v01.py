#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#  Copyright 2016 Marek Papco
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#

import gi, sys
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango
from gi.repository import GLib, Gio, Gtk

class Checker(Gtk.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="org.example.someapp",
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        self.window = None
        
        # Gio.Application
        # add_main_option(long_name, short_name, flags, arg, description, arg_description)
        self.add_main_option("test", ord("t"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Command line test", None)
        self.add_main_option("help", ord("h"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Help Command line test", None)
    
    def do_startup(self):
        Gtk.Application.do_startup(self)
        
        # static new(name, parameter_type)
        # about window
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        # quit app
        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)
        
        #hide password
        #hidepass_action = Gio.SimpleAction.new_stateful("hidepassword", None,
                                            #GLib.Variant.new_boolean(False))
        #hidepass_action.connect = ("change-state", self.on_hidepassword_toggle)
        #self.add_action(hidepass_action)
        action = Gio.SimpleAction.new("hidepassword", None)
        action.connect("activate", self.on_hidepassword)
        self.add_action(action)
        
        # Builds the user interface described by string 
        # (in the GtkBuilder UI definition format)
        builder = Gtk.Builder.new_from_file("gauthcheckermenu_v01.xml")
        
        # Gtk.Application, Sets or unsets the application menu for self
        # app_menu (Gio.MenuModel or None) in MENU_XML
        self.set_app_menu(builder.get_object("app-menu"))
    
    # hide password
    def on_hidepassword(self, action, param):
        print ("Method hide called")
        self.window.entrypass.set_visibility(False)
    
    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.window = AppWindow(application=self, title="SIP Authentication checker")

        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()

        if options.contains("test"):
            # This is printed on the main instance
            print("Test argument recieved")
        elif options.contains("help"):
            # This is printed on the main instance
            print("Help argument recieved")

        self.activate()
        return 0
    
    # hide password
    def on_hidepassword_toggle(self, action, value):
        print ("Hidepass method called")
        action.set_state(value)
        print ("Value set to" + value)
        if value.get_boolean():
            self.entrypass.set_visibility(value)
            print ("Password visibility set to:" + value)
        else:
            self.entrypass.set_visibility(value)
            print ("Password visibility set to:" + value)
    
    def on_about(self, action, param):
        #print ("About method called")
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.set_program_name("SIP authentication checker")
        about_dialog.set_authors(["Marek Papco"])
        about_dialog.set_comments("Program checks SIP messages loaded from file(s) againts provided password.")
        about_dialog.set_copyright ("Copyright 2016 Marek Papco.")
        about_dialog.set_license("Licensed under the Apache License, Version 2.0 (the \"License\") \
            \nyou may not use this file except in compliance with the License. \
            \nYou may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0")
        about_dialog.set_logo_icon_name("help-about")
        about_dialog.set_version("0.1")
            
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()

class SearchDialog(Gtk.Dialog):
    
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Search", parent,
                Gtk.DialogFlags.MODAL, buttons=(
                Gtk.STOCK_FIND, Gtk.ResponseType.OK,
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
                
        box = self.get_content_area()
    
        label = Gtk.Label("Insert text you want to search for:")
        box.add(label)
    
        self.entry = Gtk.Entry()
        box.add(self.entry)


class AppWindow(Gtk.ApplicationWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # This will be in the windows group and have the "win" prefix
        # Creates a new stateful action. new_stateful(name, parameter_type, state)
        # state (GLib.Variant) – the initial state of the action
        # maximize window
        max_action = Gio.SimpleAction.new_stateful("maximize", None,
                                            GLib.Variant.new_boolean(False))
        max_action.connect("change-state", self.on_maximize_toggle)
        # Gio.ActionMap, Adds an action to the self. action (Gio.Action)
        self.add_action(max_action)
        
        # sets default window size (width, height), (-1) - unset default width
        self.set_default_size(800, 600)
        
        # add grid
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.grid.set_column_homogeneous(True)
        self.grid.set_column_spacing(10)
        
        # textview part
        self.create_textview()
        # outview part
        self.create_outview()
        # toolbar part
        self.create_toolbar()
        # buttons part
        self.create_buttons()
        
        # generate grid, windows, everything
        self.show_all()
        
        # Keep it in sync with the actual state
        self.connect("notify::is-maximized",
                            lambda obj, pspec: max_action.set_state(
                                               GLib.Variant.new_boolean(obj.props.is_maximized)))
        
    # TOOLBAR    
    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        # attach(child, col num, row num, width, height) toolbar to grid
        self.grid.attach(toolbar, 0, 0, 6, 1)
        
        # Load File Button
        button_openfile = Gtk.ToolButton()
        button_openfile.set_icon_name("document-open")
        button_openfile.set_tooltip_text("Load file with SIP messages")
        button_openfile.connect("clicked", self.on_loadfile_clicked)
        toolbar.insert(button_openfile, 0)
 
        # Run Check Button
        button_runcheck = Gtk.ToolButton()
        button_runcheck.set_icon_name("system-run")
        button_runcheck.set_tooltip_text("Run check")
        button_runcheck.connect("clicked", self.on_run_clicked)
        toolbar.insert(button_runcheck, 1)
        
        # Password entry
        self.entrypass = Gtk.Entry()
        self.entrypass.set_text("auth password")
        self.entrypass.set_editable(True)
        self.entrypass.set_max_length(64)
        entry_password = Gtk.ToolItem()
        entry_password.add(self.entrypass)
        toolbar.insert(entry_password, 2)
        
        # add clear button
        button_clear = Gtk.ToolButton()
        button_clear.set_icon_name("edit-clear-symbolic")
        button_clear.set_tooltip_text("Clear password")
        button_clear.connect("clicked", self.on_clear_clicked)
        toolbar.insert(button_clear, 3)
        
        # add separator
        toolbar.insert(Gtk.SeparatorToolItem(), 4)
        
        # add search button
        # Gtk.SearchBar()
        searchbar = Gtk.SearchBar()
        searchbar.set_search_mode(True)
        # Gtk.Entry()
        entry_search = Gtk.SearchEntry()
        searchbar.connect_entry(entry_search)
        searchbar.add(entry_search)
        # Gtk.ToolItem (so searchentry can be added to toolbar)
        bar_search = Gtk.ToolItem()
        bar_search.add(searchbar)
        toolbar.insert(bar_search, 5)  
  
        # add separator
        toolbar.insert(Gtk.SeparatorToolItem(), 6)
        
        # add About button
        button_about = Gtk.ToolButton()
        button_about.set_icon_name("help-about")
        button_about.set_tooltip_text("About")
        #button_about.connect("clicked", self.on_about_clicked)
        toolbar.insert(button_about, 7)
        
        # a horizontal separator
        hseparator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.grid.attach(hseparator, 0, 1, 7, 1)
        
    # TEXT AREA
    def create_textview(self):
        # Expandable scrolled window
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        # attach(child, col num, row num, width, height) textview window to grid
        self.grid.attach(scrolledwindow, 0, 2, 4, 4)
        
        # add Text into scrolled window
        self.textview = Gtk.TextView()
        self.textview.set_left_margin(5)
        self.textbuffer = self.textview.get_buffer()
        self.textbuffer.set_text("SIP traces\n"
            + "text file \n"
            + "will be loaded \n"
            + "in here!")
        scrolledwindow.add(self.textview)
    
    # OUTPUT AREA
    def create_outview(self):
        # Expandable scrolled window
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        # attach(child, col num, row num, width, height) textview window to grid
        self.grid.attach(scrolledwindow, 4, 2, 4, 4)
        
        # add Text into scrolled window
        self.outview = Gtk.TextView()
        self.outview.set_left_margin(5)
        self.textbuffer = self.outview.get_buffer()
        self.textbuffer.set_text("Output\n"
            + "will \n"
            + "be \n"
            + "in here!")
        scrolledwindow.add(self.outview)
    
    # BUTTONS (bottom bar)
    def create_buttons(self):
        # a horizontal separator
        hseparator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.grid.attach(hseparator, 0, 7, 8, 1)
        # Editable
        check_editable = Gtk.CheckButton("Editable")
        check_editable.set_active(False)
        check_editable.connect("toggled", self.on_editable_toggled)
        self.grid.attach(check_editable, 0, 8, 1, 1)
        
        # No Wrapping
        radio_wrapnone = Gtk.RadioButton.new_with_label_from_widget(None, "No Wrapping")
        self.grid.attach_next_to(radio_wrapnone, check_editable,
        Gtk.PositionType.RIGHT, 1, 1)
        radio_wrapnone.connect("toggled", self.on_wrap_toggled, Gtk.WrapMode.NONE)
        # Word Wrapping
        radio_wrapword = Gtk.RadioButton.new_with_label_from_widget(radio_wrapnone, "Word Wrapping")
        self.grid.attach_next_to(radio_wrapword, radio_wrapnone, Gtk.PositionType.RIGHT, 1, 1)
        radio_wrapword.connect("toggled", self.on_wrap_toggled, Gtk.WrapMode.WORD)
        
        # Verbose Output
        check_verbose = Gtk.CheckButton("Verbose")
        check_verbose.set_active(False)
        #check_verbose.connect("toggled", self.on_verbose_toggled)
        self.grid.attach(check_verbose, 4, 8, 1, 1)
        
        # Visible Password
        check_visible = Gtk.CheckButton("Visible password")
        check_visible.set_active(True)
        check_visible.connect("toggled", self.on_visible_toggled)
        self.grid.attach_next_to(check_visible, check_verbose, Gtk.PositionType.RIGHT, 1, 1)

    # BUTTONS AUX functions    
    def on_editable_toggled(self, widget):
        self.textview.set_editable(widget.get_active())
    def on_wrap_toggled(self, widget, mode):
        self.textview.set_wrap_mode(mode)
    
    # visible password
    def on_visible_toggled(self, button):
        value = button.get_active()
        self.entrypass.set_visibility(value)
    
    # AUXILIARY FUNCTIONS
    # load file(s)
    def on_loadfile_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a file", self, \
        Gtk.FileChooserAction.OPEN,(Gtk.STOCK_CANCEL, \
        Gtk.ResponseType.CANCEL,Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        # allow more files to be selected
        dialog.set_select_multiple(True)
        
        # text filter
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            for file in dialog.get_filenames():
                print("File selected: " + file)
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()
    
    # maximize
    def on_maximize_toggle(self, action, value):
        action.set_state(value)
        if value.get_boolean():
            # Gtk.Window
            self.maximize()
        else:
            # Gtk.Window
            self.unmaximize()
            
    # clear password   
    def on_clear_clicked(self, widget):
        self.entrypass.set_text("")
        
    # run
    def on_run_clicked(self, widget):
        current_password = self.entrypass.get_text()
        print ("password: " + current_password)
        # RUN MAIN PROGRAM
    
    # search
    def on_search_clicked(self, widget):
        searchbar = Gtk.SearchBar()
        searchbar.set_search_mode(True)
        searchbar.set_show_close_button(True)
        print ("Searchbar clicked")
    
    
    ## search    
    #def on_search_clicked(self, widget):
        ## create SearchDialog object
        #dialog = SearchDialog(self)
        ## Blocks in a recursive main loop until the self either emits the Gtk.Dialog ::response signal, or is destroyed.
        #response = dialog.run()
        ## check for response
        #if response == Gtk.ResponseType.OK:
            ## Returns the mark that represents the cursor (insertion point)
            #cursor_mark = self.textbuffer.get_insert()
            ## Initializes iter with the current position of mark
            #start = self.textbuffer.get_iter_at_mark(cursor_mark)
            ## if cursor at the end of text, initialize start at text beginning
            #if start.get_offset() == self.textbuffer.get_char_count():
                #start = self.textbuffer.get_start_iter()
            
            ## Retrieves the contents of the entry widget as the input 
            ## for search_and_mark method
            #self.search_and_mark(dialog.entry.get_text(), start)

        #dialog.destroy()
    ## search and mark
    #def search_and_mark(self, text, start):
        #end = self.textbuffer.get_end_iter()
        ## Searches forward for str. Any match is returned by setting 
        ## match_start to the first character of the match and match_end 
        ## to the first character after the match. The search will not 
        ## continue past limit.
        ## forward_search(str, flags, limit)
        #match = start.forward_search(text, 0, end)

        #if match != None:
            #match_start, match_end = match
            ## apply tag from match_start to match_end
            #self.textbuffer.apply_tag(self.tag_found, match_start, match_end)
            ## continue till end
            #self.search_and_mark(text, match_end)
    
    
def main():

    app = Checker()
    # Gio.Application, Runs the application.
    exit_status = app.run(sys.argv)
    
    sys.exit(exit_status)
    
    return 0

if __name__ == '__main__':
	main()

