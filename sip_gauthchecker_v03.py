#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#  Copyright 2016 Marek Papco
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#

import gi, sys
import hashlib, re
gi.require_version('Gtk', '3.0')
#from gi.repository import Gtk, Pango
from gi.repository import GLib, Gio, Gtk

verbose = False

class Checker(Gtk.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="org.example.someapp",
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        self.window = None
        
        # Gio.Application
        # add_main_option(long_name, short_name, flags, arg, description, arg_description)
        self.add_main_option("test", ord("t"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Command line test", None)
        self.add_main_option("help", ord("h"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Help Command line test", None)
    
    def do_startup(self):
        Gtk.Application.do_startup(self)
        
        # static new(name, parameter_type)
        # about window
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        # quit app
        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)
        
        #show password
        showpass_action = Gio.SimpleAction.new_stateful("showpassword", None, GLib.Variant.new_boolean(True))
        showpass_action.connect("change-state", self.on_showpassword_toggle)
        self.add_action(showpass_action)
        
        
        # Builds the user interface described by string 
        # (in the GtkBuilder UI definition format)
        builder = Gtk.Builder.new_from_file("gauthcheckermenu_v02.xml")
        
        # Gtk.Application, Sets or unsets the application menu for self
        # app_menu (Gio.MenuModel or None) in MENU_XML
        self.set_app_menu(builder.get_object("app-menu"))
    
    # hide password
    def on_showpassword_toggle(self, showpass_action, value):
        print ("[Checker] Showpass method called")
        if value.get_boolean():
            #print ("state: ",value.get_boolean())
            self.window.entrypass.set_visibility(True)
            showpass_action.set_state(GLib.Variant.new_boolean(True))
            print ("[Checker] Show password set to ", value.get_boolean())
        else:
            #print ("state: ",value.get_boolean())
            self.window.entrypass.set_visibility(False)
            showpass_action.set_state(GLib.Variant.new_boolean(False))
            print ("[Checker] Show password set to ", value.get_boolean())
    
    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.window = AppWindow(application=self, title="SIP Authentication checker")

        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()

        if options.contains("test"):
            # This is printed on the main instance
            print("Test argument recieved")
        elif options.contains("help"):
            # This is printed on the main instance
            print("Help argument recieved")

        self.activate()
        return 0
    
    def on_about(self, action, param):
        #print ("About method called")
        dialog_about = Gtk.AboutDialog(transient_for=self.window, modal=True)
        dialog_about.set_program_name("SIP authentication checker")
        dialog_about.set_authors(["Marek Papco"])
        dialog_about.set_comments("Program checks SIP messages loaded from file(s) againts provided password.")
        dialog_about.set_copyright ("Copyright 2016 Marek Papco.")
        dialog_about.set_license("Licensed under the Apache License, Version 2.0 (the \"License\") \
            \nyou may not use this file except in compliance with the License. \
            \nYou may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0")
        dialog_about.set_logo_icon_name("help-about")
        dialog_about.set_version("0.3")
            
        dialog_about.present()

    def on_quit(self, action, param):
        self.quit()

class SearchDialog(Gtk.Dialog):
    
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Search", parent,
                Gtk.DialogFlags.MODAL, buttons=(
                Gtk.STOCK_FIND, Gtk.ResponseType.OK,
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
                
        box = self.get_content_area()
    
        label = Gtk.Label("Insert text you want to search for:")
        box.add(label)
    
        self.entry = Gtk.Entry()
        box.add(self.entry)
        
        self.show_all()


class AppWindow(Gtk.ApplicationWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # This will be in the windows group and have the "win" prefix
        # Creates a new stateful action. new_stateful(name, parameter_type, state)
        # state (GLib.Variant) – the initial state of the action
        # maximize window
        max_action = Gio.SimpleAction.new_stateful("maximize", None,
                                            GLib.Variant.new_boolean(False))
        max_action.connect("change-state", self.on_maximize_toggle)
        # Gio.ActionMap, Adds an action to the self. action (Gio.Action)
        self.add_action(max_action)
        
        # sets default window size (width, height), (-1) - unset default width
        self.set_default_size(800, 600)
        
        # add grid
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.grid.set_column_homogeneous(True)
        self.grid.set_column_spacing(10)
        
        # textview part
        self.create_textview()
        # outview part
        self.create_outview()
        # toolbar part
        self.create_toolbar()
        # buttons part
        self.create_buttons()
        
        # generate grid, windows, everything
        self.show_all()
        
        # Keep it in sync with the actual state
        self.connect("notify::is-maximized",
                            lambda obj, pspec: max_action.set_state(
                                               GLib.Variant.new_boolean(obj.props.is_maximized)))
        
    # TOOLBAR    
    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        # attach(child, col num, row num, width, height) toolbar to grid
        self.grid.attach(toolbar, 0, 0, 6, 1)
        
        # Load File Button
        button_openfile = Gtk.ToolButton()
        button_openfile.set_icon_name("document-open")
        button_openfile.set_tooltip_text("Load file with SIP messages")
        button_openfile.connect("clicked", self.on_loadfile_clicked)
        toolbar.insert(button_openfile, 0)
 
        # Run Check Button
        button_runcheck = Gtk.ToolButton()
        button_runcheck.set_icon_name("system-run")
        button_runcheck.set_tooltip_text("Run check")
        button_runcheck.connect("clicked", self.on_run_clicked)
        toolbar.insert(button_runcheck, 1)
        
        # Password entry
        self.entrypass = Gtk.Entry()
        self.entrypass.set_text("auth password")
        self.entrypass.set_tooltip_text("Password used for response computation")
        self.entrypass.set_editable(True)
        self.entrypass.set_max_length(64)
        entry_password = Gtk.ToolItem()
        entry_password.add(self.entrypass)
        toolbar.insert(entry_password, 2)
        
        # add clear button
        button_clear = Gtk.ToolButton()
        button_clear.set_icon_name("edit-clear-symbolic")
        button_clear.set_tooltip_text("Clear password")
        button_clear.connect("clicked", self.on_clear_clicked)
        toolbar.insert(button_clear, 3)
        
        # add separator
        toolbar.insert(Gtk.SeparatorToolItem(), 4)
        
        # Gtk.SearchBar()
        searchbar = Gtk.SearchBar()
        searchbar.set_search_mode(True)
        # Gtk.SearchEntry()
        entry_search = Gtk.SearchEntry()
        entry_search.connect("search-changed", self.search_changed)
        searchbar.connect_entry(entry_search)
        searchbar.add(entry_search)
        searchbar.set_tooltip_text("Find entered text in SIP message")
        # Gtk.ToolItem (so searchentry can be added to toolbar)
        bar_search = Gtk.ToolItem()
        bar_search.add(searchbar)
        toolbar.insert(bar_search, 5)
        
        # Search button
        #button_search = Gtk.ToolButton()
        #button_search.set_icon_name("system-search")
        #button_search.connect("clicked", self.on_search_clicked)
        #toolbar.insert(button_search, 6)
  
        # add separator
        toolbar.insert(Gtk.SeparatorToolItem(), 6)
        
        # add help button
        button_about = Gtk.ToolButton()
        button_about.set_icon_name("help-contents")
        button_about.set_tooltip_text("Usage")
        button_about.connect("clicked", self.on_help_clicked)
        toolbar.insert(button_about, 7)
        
        # a horizontal separator
        hseparator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.grid.attach(hseparator, 0, 1, 7, 1)
    
    # TEXT AREA
    def create_textview(self):
        # Expandable scrolled window
        self.inwindow = Gtk.ScrolledWindow()
        self.inwindow.set_hexpand(True)
        self.inwindow.set_vexpand(True)
        # attach(child, col num, row num, width, height) textview window to grid
        self.grid.attach(self.inwindow, 0, 2, 4, 4)
        
        # add Text into scrolled window
        self.intextview = Gtk.TextView()
        self.intextview.set_left_margin(5)
        self.intextview.set_editable(False)
        self.intextbuffer = self.intextview.get_buffer()
        self.intextbuffer.set_text("SIP traces\n"
            + "text file \n"
            + "will be loaded \n"
            + "in here!")
        self.inwindow.add(self.intextview)
        
        self.tag_found = self.intextbuffer.create_tag("found", background="yellow")
    
    # OUTPUT AREA
    def create_outview(self):
        # Expandable scrolled window
        self.outwindow = Gtk.ScrolledWindow()
        self.outwindow.set_hexpand(True)
        self.outwindow.set_vexpand(True)
        # attach(child, col num, row num, width, height) textview window to grid
        self.grid.attach(self.outwindow, 4, 2, 4, 4)
        
        # add Text into scrolled window
        self.outtextview = Gtk.TextView()
        #self.outtextview.set_monospace(True)
        self.outtextview.set_left_margin(5)
        self.outtextbuffer = self.outtextview.get_buffer()
        self.outtextbuffer.set_text("Output\n"
            + "will \n"
            + "be \n"
            + "in here!")
        self.outwindow.add(self.outtextview)
    
    # BUTTONS (bottom bar)
    def create_buttons(self):
        # a horizontal separator
        hseparator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.grid.attach(hseparator, 0, 7, 8, 1)
        # Editable
        check_editable = Gtk.CheckButton("Editable")
        check_editable.set_active(False)
        check_editable.connect("toggled", self.on_editable_toggled)
        self.grid.attach(check_editable, 0, 8, 1, 1)
        
        # No Wrapping
        radio_wrapnone = Gtk.RadioButton.new_with_label_from_widget(None, "No Wrapping")
        self.grid.attach_next_to(radio_wrapnone, check_editable,
        Gtk.PositionType.RIGHT, 1, 1)
        radio_wrapnone.connect("toggled", self.on_wrap_toggled, Gtk.WrapMode.NONE)
        # Word Wrapping
        radio_wrapword = Gtk.RadioButton.new_with_label_from_widget(radio_wrapnone, "Word Wrapping")
        self.grid.attach_next_to(radio_wrapword, radio_wrapnone, Gtk.PositionType.RIGHT, 1, 1)
        radio_wrapword.connect("toggled", self.on_wrap_toggled, Gtk.WrapMode.WORD)
        
        # Verbose Output
        check_verbose = Gtk.CheckButton("Verbose")
        check_verbose.set_active(False)
        check_verbose.connect("toggled", self.on_verbose_toggled)
        self.grid.attach(check_verbose, 4, 8, 1, 1)

    # BUTTONS AUX functions    
    def on_editable_toggled(self, widget):
        self.intextview.set_editable(widget.get_active())
        print ("[AppWindow] Editable changed to", widget.get_active())
    def on_wrap_toggled(self, widget, mode):
        self.intextview.set_wrap_mode(mode)
        print ("[AppWindow] Wrapping set to", self.intextview.get_wrap_mode())
    def on_verbose_toggled(self, widget):
        global verbose
        verbose = widget.get_active()
        if verbose : print ("[AppWindow] Verbose set to ", widget.get_active())
    
    # AUXILIARY FUNCTIONS
    # load file(s)
    def on_loadfile_clicked(self, widget):
        dialog_file = Gtk.FileChooserDialog("Please choose a file", self, \
        Gtk.FileChooserAction.OPEN,(Gtk.STOCK_CANCEL, \
        Gtk.ResponseType.CANCEL,Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        # allow more files to be selected
        dialog_file.set_select_multiple(False)
        
        # text filter
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog_file.add_filter(filter_text)

        response = dialog_file.run()
        if response == Gtk.ResponseType.OK:
            if verbose : print("\n[AppWindow] Open clicked")
            #for file in dialog.get_filenames(): 
            #   above used if  set_select_multiple is True
            #   print("File selected: " + file)
            if verbose : print("[AppWindow] File selected: " + dialog_file.get_filename())
            # Load file content
            infile = open(dialog_file.get_filename(), 'r')
            intxt = infile.read()
            infile.close()
            self.intextbuffer.set_text(str(intxt))
            
        elif response == Gtk.ResponseType.CANCEL:
            if verbose : print("[AppWindow] Cancel clicked")

        dialog_file.destroy()
    
    # maximize
    def on_maximize_toggle(self, action, value):
        action.set_state(value)
        if value.get_boolean():
            # Gtk.Window
            self.maximize()
        else:
            # Gtk.Window
            self.unmaximize()
            
    # clear password   
    def on_clear_clicked(self, widget):
        self.entrypass.set_text("")
        
    # run
    def on_run_clicked(self, widget):
        # RUN MAIN PROGRAM
        current_password = self.entrypass.get_text()
        if verbose : print ("[AppWindow] password loaded")
        
        # Gtk.TextBuffer, 
        # get_bounds() retrieves the first and last iterators in the buffer
        start, end = self.intextview.get_buffer().get_bounds()
        # get_text(start, end, include_hidden_chars)
        to_analyze = self.intextview.get_buffer().get_text(start, end, True)
        #print ("[AppWindow] INPUT :\n", to_analyze)
        
        # Clear output text window from notes
        self.outtextbuffer.set_text("")
        
        # SIPParser - create parser object and start parsing
        sipparser = SIPParser(to_analyze)
        sipparser.parse_input()
        
        # ResponseCalculator - calculate response and updates cresponse
        # value in SIPMessage
        rescalculator = ResponseCalculator(current_password, self.outtextbuffer)
        if verbose: print ("[AppWindow] Address of method_list via get: ", sipparser.get_method_list())
        rescalculator.compute_response(sipparser.get_method_list())
        # destroy ResponseCalculator object
        del rescalculator
        
        # destroy SIPParser object
        del sipparser
    
    # search
    def search_changed(self, entrysearch):
        if verbose : print (entrysearch.get_text())
        # Get start and end text position
        start = self.intextbuffer.get_start_iter()
        end = self.intextbuffer.get_end_iter()
        if entrysearch.get_text():
            self.search_and_mark(entrysearch.get_text(), start)
        else:
            self.intextbuffer.remove_all_tags(start, end)
    # search and mark
    def search_and_mark(self, text, start):
        end = self.intextbuffer.get_end_iter()
        # Searches forward for str. Any match is returned by setting 
        # match_start to the first character of the match and match_end 
        # to the first character after the match. The search will not 
        # continue past limit.
        # forward_search(str, flags, limit)
        
        match = start.forward_search(text, 0, end)

        if match != None:
            match_start, match_end = match
            # apply tag from match_start to match_end
            self.intextbuffer.apply_tag(self.tag_found, match_start, match_end)
            # continue till end
            self.search_and_mark(text, match_end)
    
    
    ## OLD search (using search button that opens search dialog)    
    #def on_search_clicked(self, widget):
        ## create SearchDialog object
        #dialog = SearchDialog(self)
        ## Blocks in a recursive main loop until the self either emits the Gtk.Dialog ::response signal, or is destroyed.
        #response = dialog.run()
        ## check for response
        #if response == Gtk.ResponseType.OK:
            ## Returns the mark that represents the cursor (insertion point)
            #cursor_mark = self.intextbuffer.get_insert()
            ## Initializes iter with the current position of mark
            #start = self.intextbuffer.get_iter_at_mark(cursor_mark)
            ## if cursor at the end of text, initialize start at text beginning
            #if start.get_offset() == self.intextbuffer.get_char_count():
                #start = self.intextbuffer.get_start_iter()
            
            ## Retrieves the contents of the entry widget as the input 
            ## for search_and_mark method
            #self.search_and_mark(dialog.entry.get_text(), start)

        #dialog.destroy()
    
    # open usage window
    def on_help_clicked(self, widget):
        dialog_help = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "Usage")
        dialog_help.format_secondary_text(
        "Load file using left most icon at top bar.\
        \nEnter password in password field (one with the filled \"auth password\"). \
        \nPress run button and check results of response computation on the right side of window.\
        \nFor more verbose output tick \"Verbose\"button.\
        \nFor searching of specific pattern in SIP messages use search bar at the top.\
        ")
        dialog_help.run()
        print("Help dialog closed")
        dialog_help.destroy()
        
    
class SIPParser():
    def __init__(self, inputtext):
        if verbose : print ("[SIPParser] Calling constructor")
        self.inputtext = inputtext
        self.list_methods = []
        #print("inputtext: ", inputtext)
        
    def __del__(self):
        class_name = self.__class__.__name__
        if verbose : print ("[SIPParser] Object destroyed")
        
    # Adding message to list
    def add_method_tolist(self, p_sipmessage):
        self.list_methods.append(p_sipmessage)
        if verbose : print ("[SIPParser] Object sipmessage added to list: ", p_sipmessage)
        if verbose : print ("[SIPParser] methods in list: ", len(self.list_methods))
        #print ("[add_method_tolist] Methods list memory address: ", self.list_methods)
        
    # Get list of methods
    def get_method_list(self):
        #print ("[SIPParser] Methods list memory address: ", self.list_methods)
        return self.list_methods
    
    # Parse input    
    def parse_input(self):
        i = 0
          
        intext = self.inputtext.split('\n')

        for line in intext:
            line=line.strip() # removes white characters from line
            #print (line)
            i += 1 # remember line
            match = re.match(r'REGISTER|INVITE',line)
            if (match):
                method = match.group(0)
                if verbose: print ("\n[SIPParser] Method -> " + match.group(0))
                self.parse_method(intext, i)
    
    # Parse SIP METHOD (REGISTER or INVITE)
    def parse_method(self, intext, line_i):
 
        callid = None
        cseq = None
        mname = None
        uname = None
        realm = None
        nonce = None
        uri = None
        response = None
        auth_flag = False
        algorithm = None
       
        if (verbose): print ("[SIPParser] Parsing method")
     
        while (intext[line_i].strip() != ""):
            #print (intext[line_i].rstrip())
            intext[line_i] = intext[line_i].lstrip()
            # parse Call-ID header
            if ('Call-ID:' in intext[line_i]):
                match = re.match(r'Call-ID:\s+(\S+)\s*|i:\s+(\S+)\s*',intext[line_i])
                if verbose : print ("[SIPParser]    Call ID -> " + match.group(1))
                callid = match.group(1)
            # parse CSeq header
            elif ('CSeq:' in intext[line_i]):
                match = re.match(r'CSeq:\s+(\S+)\s+(\S+)',intext[line_i])
                if verbose : print ("[SIPParser]    CSeq -> " + match.group(1))
                #if verbose : print ("[SIPParser] Method -> " + match.group(2))
                cseq = match.group(1)
                mname = match.group(2)
            # parse Authorization header
            elif ('Authorization:' in intext[line_i]):
                match = re.match(r'(Authorization:|Proxy-Authorization:)\sDigest\s(.+)', intext[line_i])
                if verbose : print ("[SIPParser]    Auth header -> " + match.group(0))
                auth_flag = True
                credentials = []
                credentials = re.split('\s',match.group(2))
                if (len(credentials) > 2):
                    for item in credentials:
                        if ('username' in item):
                            match = re.search('username=\"(\S+)\"', item)
                            uname = match.group(1)
                            if verbose : print ("[SIPParser]     username: " + uname)
                        elif ('realm' in item):
                            match = re.search('realm=\"(\S+)\"', item)
                            realm = match.group(1)
                            if verbose : print ("[SIPParser]     realm: " + realm)
                        elif ('nonce' in item):
                            match = re.search('nonce=\"(\S+)\"', item)
                            nonce = match.group(1)
                            if verbose : print ("[SIPParser]     nonce: " + nonce)
                        elif ('uri' in item):
                            match = re.search('uri=\"(\S+)\"', item)
                            uri = match.group(1)
                            if verbose : print ("[SIPParser]     uri: " + uri)
                        elif ('response' in item):
                            match = re.search('response=\"(\S+)\"', item)
                            response = match.group(1)
                            if verbose : print ("[SIPParser]     response: " + nonce)
                        elif ('algorithm' in item):
                            match = re.search('algorithm=(\S+)', item)
                            algorithm = match.group(1)
                            if verbose : print ("[SIPParser]     algorithm: " + nonce)
                        else:
                            print ("[SIPParser]  Parameter [" + item + "] unknown!" )
            
            else:
                if verbose : print ("[SIPParser] NA -> " + intext[line_i].rstrip())
        
            line_i += 1 # increase line index
        
        if verbose : print ("[SIPParser] End of method!\n")
        
        if (auth_flag):
            # if 'algorithm' parameter not present, set to MD5
            if (algorithm == None) : algorithm='MD5'
            if verbose : print ("[SIPParser] Calling SIPMessage constructor.")
            
            # SIPMessage - create SIP method object and initialize method parameters
            sipm = SIPMessage(mname, callid, cseq, uname, realm, nonce, uri, response, algorithm)
            
            # if check is successfull add method to list
            # otherwise delete object
            if (sipm.check_method()):
                self.add_method_tolist(sipm)
            else:
                if verbose : print ("[SIPParser] Check of mandatory parameters failed, \
                destroying object!")
                del sipm
            
        else:
            if verbose : print ("[SIPParser] No Authorization header found, skipping method")
        
        return 0

class SIPMessage():
    def __init__(self, mname, callid, cseq, uname, realm, nonce, uri, response, alg):
        if verbose : print ("[SIPMessage] Calling " + mname + " constructor")
        self.mname = mname
        self.callid = callid
        self.cseq = cseq
        self.uname = uname
        self.realm = realm
        self.nonce = nonce
        self.uri = uri
        self.response = response
        self.alg = alg
        self.cresponse = None
        
    def __del__(self):
        class_name = self.__class__.__name__
        if verbose : print ("[SIPMessage] " + class_name + " " + self.mname + " destroyed") 
        
    def setcresponse(self, cresponse):
        self.cresponse = cresponse
    
    # check for mandatory parameters
    def check_method(self):
        if verbose : print ("[SIPMessage] Checking if mandatory parameters are initialized.")
        if (self.callid == None) : 
            print ("[SIPMessage] Mandatory parameter \"Call-ID\" not found!")
            return -1
        elif(self.cseq == None) : 
            print ("[SIPMessage] Mandatory parameter \"Cseq\" not found!")
            return -1
        elif(self.mname == None) : 
            print ("[SIPMessage] Mandatory parameter \"Method name\" not found!")
            return -1
        elif(self.uname == None) : 
            print ("[SIPMessage] Mandatory parameter \"uname\" not found!")
            return -1
        elif(self.realm == None) : 
            print ("[SIPMessage] Mandatory parameter \"realm\" not found!")
            return -1
        elif(self.nonce == None) : 
            print ("[SIPMessage] Mandatory parameter \"nonce\" not found!")
            return -1
        elif(self.uri == None) : 
            print ("[SIPMessage] Mandatory parameter \"uri\" not found!")
            return -1
        elif(self.response == None) : 
            print ("[SIPMessage] Mandatory parameter \"response\" not found!")
            return -1
        elif(self.alg == None) : 
            print ("[SIPMessage] Mandatory parameter \"algorithm\" not found!")
            return -1
        else:
            # check for mandatory parameters successful
            if verbose : print ("[SIPMessage] Mandatory parameters test successful.")
            return 1
            
        return 0

class ResponseCalculator():
    
    def __init__(self, password, outputbuffer):
        if verbose : print ("[ResponseCalculator] Calling constructor")
        self.secret = password
        self.outputbuffer = outputbuffer
        #print("password: ", password)
        
    def __del__(self):
        class_name = self.__class__.__name__
        if verbose : print ("[ResponseCalculator] Object destroyed")
    
        
    def compute_response(self, method_list):
    
        #print ("Secret : ", self.secret)
        #print ("[ResponseCalculator] Address of methods list: ", method_list)
        
        for method in method_list:
            # if alg == <alg-sess>
            #       A1 = H (uname:realm:pass):nonce:cnonce
            # else 
            #       A1 = H (uname:realm:pass)
            #
            # is userhash = True 
            #       uname = H (uname:realm), ask for username
            #
    
            print ("\n[Response Calculator] Computing response for " + 
            getattr(method, 'mname') + ' CSeq: ' +  getattr(method, 'cseq') + 
            " Call-ID: " + getattr(method, 'callid'))
            
            end_iter = self.outputbuffer.get_end_iter()
            text = str (
            "Computing response for " \
            + "method: " + getattr(method, 'mname') \
            + "\n\t CSeq num: " +  getattr(method, 'cseq') \
            + "\n\t Call-ID: " + getattr(method, 'callid') \
            + "\n")
            self.outputbuffer.insert(end_iter, text)
            
            # A1 = uname + ':' + realm + ':' + upass
            A1 = getattr(method, 'uname') + ':' + getattr(method, 'realm') + ':' + self.secret 
            # HA1 
            HA1 = hashlib.md5(A1.encode())
            #A2 = method + ":" + ruri
            A2 = getattr(method, 'mname') + ':' + getattr(method, 'uri')
            # HA2
            HA2 = hashlib.md5(A2.encode())            
            #response = ha1.hexdigest() + ':' + nonce + ':' + ha2.hexdigest()
            cresponse = HA1.hexdigest() + ':' + getattr(method, 'nonce') + ':' + HA2.hexdigest()
            #hresponse = hashlib.md5(response.encode())
            hresponse =  (hashlib.md5(cresponse.encode())).hexdigest()
            method.setcresponse(hresponse)
            
            if verbose : 
                print ("[COMPUTE RESPONSE]    A1: ", A1) 
                print ("[COMPUTE RESPONSE]    HA1: ", HA1.hexdigest())
                print ("[COMPUTE RESPONSE]    A2: ", A2)
                print ("[COMPUTE RESPONSE]    HA2: ", HA2.hexdigest())
                print ("[COMPUTE RESPONSE]    Composed response: ", cresponse) 
            
                end_iter = self.outputbuffer.get_end_iter()
                text = str ("\t A1: " + A1 + "\n\t HA1: " + HA1.hexdigest() \
                + "\n\t A2: " + A2 + "\n\t HA2: " + HA2.hexdigest() \
                + "\n\t Composed response: " + cresponse
                + "\n")
                self.outputbuffer.insert(end_iter, text)
                
                
            
            print ("[COMPUTE RESPONSE]    Username:                 ", getattr(method, 'uname'))
            print ("[COMPUTE RESPONSE]    Computed hashed response: ", hresponse)
            print ("[COMPUTE RESPONSE]    Sent response:            ", getattr(method, 'response'))
            
            end_iter = self.outputbuffer.get_end_iter()
            text = str (
            "\t Username:                 " + getattr(method, 'uname') \
            + "\n\t Computed response: " + "{0:>34}".format(hresponse) \
            + "\n\t Sent response:            " + "{0:>32}".format(getattr(method, 'response'))
            )
            self.outputbuffer.insert(end_iter, text)
            
            if (hresponse == getattr(method, 'response')):
                end_iter = self.outputbuffer.get_end_iter()
                text = str ("\nMATCH FOUND!")
                self.outputbuffer.insert(end_iter, text)
            end_iter = self.outputbuffer.get_end_iter()
            text = str ("\n\n")
            self.outputbuffer.insert(end_iter, text)
            
        return 0

def main():

    app = Checker()
    # Gio.Application, Runs the application.
    exit_status = app.run(sys.argv)
    
    sys.exit(exit_status)
    
    return 0

if __name__ == '__main__':
	main()

